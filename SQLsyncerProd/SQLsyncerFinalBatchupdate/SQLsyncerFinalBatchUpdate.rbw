#This program serves as media for transferring data from ProdData table of MS Access database to SQL database table.
#It creates and updates all records from MS Access to SQL.
#All created and updated records can be seen on the logfile


require 'win32ole'
require 'rubygems'
require 'active_record'
require 'activerecord-sqlserver-adapter'
require 'tiny_tds'
require 'log4r'
require 'win32API'
require 'net/smtp'
require 'mail'


#Log info and error during the program run
Log = Log4r::Logger.new("LogProdData")
Log.outputters << Log4r::Outputter.stderr
Dir.mkdir('log') unless Dir.exist?('log')
file = Log4r::FileOutputter.new('LogProdData',:filename=>'log/LogProdData.log')
file.formatter = Log4r::PatternFormatter.new(:pattern=>"[%l] %d :: %m")
Log.outputters << file



SW_HIDE = 0
WS_OVERLAPPED = 0
WS_DISABLED = 0x08000000
WS_MINIMIZED = 0x20000000

def already_running
	
	findwindow = Win32API.new('user32','FindWindow', ['p','p'], 'L')
	return true if findwindow.call("STATIC", "Rhi4FileProcessorHiddenWindowProd") != 0
	createwindow = Win32API.new('user32', 'CreateWindowEx', ['l', 'p','p', 'l', 'i', 'i', 'i', 'i', 
		'l', 'l', 'l', 'P'], 'l')
	dwStyle = (WS_DISABLED | WS_MINIMIZED)
	cw = createwindow.call( WS_OVERLAPPED, 'STATIC','Rhi4FileProcessorHiddenWindowProd', dwStyle, 0, 0, 0, 0, 0, 0, 0, 0 )
	show = Win32API.new('user32',  'ShowWindow', ['L', 'L'], 'L')
	show.call(cw, SW_HIDE)
	return false
end



if already_running
	Log.info "Sorry Application is already running!"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	exit
end



#YAML Configuration
begin

	@config=YAML.load_file Dir.pwd+'/config.yml'	
	
	
rescue Exception => e
	Log.error "Unable to find 'config.yml'"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	exit
end



# Email Notification set up for error
$recipient = @config['to']
$sender = @config['from']
$postemail = @config['postmaster']
$postpassword = @config['postpassword']


smtp = Net::SMTP.new 'smtp.gmail.com', 587
smtp.enable_starttls
smtp.start($postemail,$postemail,$postpassword,:login) do



#Set up MSaccess connection
$table_n=@config['tablename']
$primary_k=@config['primarykey']

class AccessDb
    attr_accessor :mdb, :connection, :data, :fields

    def initialize(mdb=nil)
        @mdb = mdb
        @connection = nil
        @data = nil
        @fields = nil
    end

    def open 
        connection_string =  'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='
        connection_string << @mdb
        @connection = WIN32OLE.new('ADODB.Connection')
        @connection.Open(connection_string)
    end

    def query(sql)
        recordset = WIN32OLE.new('ADODB.Recordset')
        recordset.Open(sql, @connection)
        @fields = []
        recordset.Fields.each do |field|
            @fields << field.Name
        end
        begin
            @data = recordset.GetRows.transpose
        rescue
            @data = []
        end
        recordset.Close
    end

    def execute(sql)
        @connection.Execute(sql)
    end

    def close
        @connection.Close
    end
end



begin

	db = AccessDb.new(@config['access_path'])
	db.open
		
		#Check number of records in MS ACCESS,Reference for record creation in sql
		db.query("SELECT * FROM #{@config['proddata']} WHERE a_created_at is null ORDER BY Id")
			field_names_all = db.fields
			rows_all = db.data
			
		#update query
		db.query("SELECT * FROM #{@config['proddata']} WHERE Status<>'5' or Status is null ORDER BY Id")
			field_update = db.fields
			rows_update = db.data
			
		#complete query
		db.query("SELECT * FROM #{@config['proddata']} WHERE Status='5' and CompleteSync is null ORDER BY Id")
			field_complete = db.fields
			rows_complete = db.data

			
	rescue Exception => e
		Log.error "Could Not Connect to MS Access Database"
		Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
		$msg = "Subject: ProdData SQL Syncer Error \n\n\n #{e}.\n Cannot connect to MS Access Database. \n\n Date & Time: #{Time.now}"
		smtp.send_message($msg, $sender, $recipient)
		exit
end


#Connect to SQL Server		
ActiveRecord::Base.default_timezone = :local
ActiveRecord::Base.establish_connection(
	
	:adapter=>"sqlserver",
	:mode=>'dblib',
	:host=>@config['host'],
	:port=>@config['port'],
	:database=>@config['database'],
	:username=>@config['username'],
	:password=>@config['password'],
	:timeout=>@config['timeout'],
	:azure=>'false'

)

# Define Active records for SQL	
class Boo < ActiveRecord::Base

	self.table_name=$table_n
	self.primary_key=$primary_k

	
end



begin
Boo.connection
rescue Exception => e
	Log.error "Could Not Connect to SQL Server"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Could not connect to SQL Server \n\n Date & Time: #{Time.now}"
	smtp.send_message($msg, $sender, $recipient)
	exit
end




def SQLBatchUpdate(SQLDateTimeUpdated,accessData,accessdb)

begin
	if !accessdate.nil? and !accesstime.nil? 
		gendatearr=accessdate.strftime("%Y/%m/%d").split("/")#datetime month day yr as array
		gentime=accesstime.split(":")
		gendate = Date.new(gendatearr[0].to_i, gendatearr[1].to_i, gendatearr[2].to_i)
		dtgen = DateTime.new(gendate.year, gendate.month, gendate.day,gentime[0].to_i,gentime[1].to_i,0).change(:offset => "+0800")
		genupdate = Boo.find_by(SQL_ID:updatedbaccess[0])
	
		updatedatprev=genupdate.updated_at
  
		if dtgen>updatedatprev
				
				genupdate.update(SQL_ID:updatedbaccess[0],Arr_Date:updatedbaccess[1],Arr_Time:updatedbaccess[2],Variety_Code:updatedbaccess[3],Variety:updatedbaccess[4],MTicket:updatedbaccess[5],CDT_no:updatedbaccess[6],Q_No:updatedbaccess[7],SGuideNo:updatedbaccess[8],Truck_No:updatedbaccess[9],Trucker:updatedbaccess[10],Carrier_Type:updatedbaccess[11],Truck_spec:updatedbaccess[12],Van_No:updatedbaccess[13],Planter_Code:updatedbaccess[14],Barrio_Code:updatedbaccess[15],Driver_Code:updatedbaccess[16],GW_Date:updatedbaccess[17],GW_Time:updatedbaccess[18],Gross_Weight:updatedbaccess[19],Gross_Scaler:updatedbaccess[20],Original_GrossW:updatedbaccess[21],TW_Date:updatedbaccess[22],TW_Time:updatedbaccess[23],Tare_Weight:updatedbaccess[24],Tare_Scaler:updatedbaccess[25],SSCR_Date:updatedbaccess[26],SSCR_Time:updatedbaccess[27],Trash_percent:updatedbaccess[28],Burnt:updatedbaccess[29],Ded_Leaves:updatedbaccess[30],Ded_CaneTops:updatedbaccess[31],Ded_DeadStalks:updatedbaccess[32],Ded_WaterShoots:updatedbaccess[33],Ded_Muddy:updatedbaccess[34],Mill_No:updatedbaccess[35],Sample_No:updatedbaccess[36],Cane_Quality:updatedbaccess[37],Darami:updatedbaccess[38],Fejas_Date:updatedbaccess[39],Fejas_Time:updatedbaccess[40],Cor_Brix:updatedbaccess[41],POL_Reading:updatedbaccess[42],POL_percent:updatedbaccess[43],APRTY:updatedbaccess[44],PSTC:updatedbaccess[45],KSTC:updatedbaccess[46],LKGTC:updatedbaccess[47],Planters_Sugar:updatedbaccess[48],Planters_Molasses:updatedbaccess[49],Status:updatedbaccess[50],Carrier:updatedbaccess[51],Pending:updatedbaccess[52],Changed:updatedbaccess[53],Spillage:updatedbaccess[54],Spill_Code:updatedbaccess[55],SFactor:updatedbaccess[56],MFactor:updatedbaccess[57],Mode:updatedbaccess[58],Abono:updatedbaccess[59],Remarks:updatedbaccess[60],Reanalyzed_Date:updatedbaccess[61],CA:updatedbaccess[62],Type:updatedbaccess[63],Release_Date:updatedbaccess[64],Field_No:updatedbaccess[65],Dump_Location:updatedbaccess[66],Fiber:updatedbaccess[67],Retension:updatedbaccess[68],Extraction:updatedbaccess[69],AbsJuice:updatedbaccess[70],PolPercentCane:updatedbaccess[71],Final:updatedbaccess[72],Tag:updatedbaccess[73],Mill_Date:updatedbaccess[74],Mill_Time:updatedbaccess[75],Mill:updatedbaccess[76],SeqNo:updatedbaccess[77])
		
				accessid=genupdate.SQL_ID
				updatedatcur=genupdate.updated_at
				accessdb.execute("UPDATE #{@config['proddata']} SET a_updated_at='#{updatedatcur.to_s}' WHERE id=#{accessid};")
				Log.info "updating SQL_ID #{accessid}"
			 
		end
	end
	
	rescue Exception => e
					Log.error "Error updating SQL record."
					Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
					#$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Failure  to update SQL/Access record with SQL_ID #{accessid}. \n\n Date & Time: #{Time.now}"
					#smtp.send_message($msg, $sender, $recipient)
					exit
end

end
 
   
for i in 0..(rows_complete.length-1)

	accessRecord=rows_complete[i]
	sqlRecord=Boo.find_by(SQL_ID:accessRecord[0])
	SQLBatchUpdate(sqlRecord.updated_at,accessrecord,db)
end
  
db.close

end

Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"





