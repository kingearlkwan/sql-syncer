#This program serves as media for transferring data from ProdData table of MS Access database to SQL database table.
#It creates and updates all records from MS Access to SQL.
#All created and updated records can be seen on the logfile


require 'win32ole'
require 'rubygems'
require 'active_record'
require 'activerecord-sqlserver-adapter'
require 'tiny_tds'
require 'log4r'
require 'win32API'
require 'net/smtp'
require 'mail'
require 'date'


#Log info and error during the program run
Log = Log4r::Logger.new("LogProdData")
Log.outputters << Log4r::Outputter.stderr
Dir.mkdir('log') unless Dir.exist?('log')
file = Log4r::FileOutputter.new('LogProdData',:filename=>'log/LogProdData.log')
file.formatter = Log4r::PatternFormatter.new(:pattern=>"[%l] %d :: %m")
Log.outputters << file



SW_HIDE = 0
WS_OVERLAPPED = 0
WS_DISABLED = 0x08000000
WS_MINIMIZED = 0x20000000

def already_running
	
	findwindow = Win32API.new('user32','FindWindow', ['p','p'], 'L')
	return true if findwindow.call("STATIC", "Rhi4FileProcessorHiddenWindowProd") != 0
	createwindow = Win32API.new('user32', 'CreateWindowEx', ['l', 'p','p', 'l', 'i', 'i', 'i', 'i', 
		'l', 'l', 'l', 'P'], 'l')
	dwStyle = (WS_DISABLED | WS_MINIMIZED)
	cw = createwindow.call( WS_OVERLAPPED, 'STATIC','Rhi4FileProcessorHiddenWindowProd', dwStyle, 0, 0, 0, 0, 0, 0, 0, 0 )
	show = Win32API.new('user32',  'ShowWindow', ['L', 'L'], 'L')
	show.call(cw, SW_HIDE)
	return false
end



if already_running
	Log.info "Sorry Application is already running!"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	exit
end



#YAML Configuration
begin

	@config=YAML.load_file Dir.pwd+'/config.yml'	
	
	
rescue Exception => e
	Log.error "Unable to find 'config.yml'"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	exit
end



# Email Notification set up for error
$recipient = @config['to']
$sender = @config['from']
$postemail = @config['postmaster']
$postpassword = @config['postpassword']


smtp = Net::SMTP.new 'smtp.gmail.com', 587
smtp.enable_starttls
smtp.start($postemail,$postemail,$postpassword,:login) do



#Set up MSaccess connection
$table_n=@config['tablename']
$primary_k=@config['primarykey']

class AccessDb
    attr_accessor :mdb, :connection, :data, :fields

    def initialize(mdb=nil)
        @mdb = mdb
        @connection = nil
        @data = nil
        @fields = nil
    end

    def open 
        connection_string =  'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='
        connection_string << @mdb
        @connection = WIN32OLE.new('ADODB.Connection')
        @connection.Open(connection_string)
    end

    def query(sql)
        recordset = WIN32OLE.new('ADODB.Recordset')
        recordset.Open(sql, @connection)
        @fields = []
        recordset.Fields.each do |field|
            @fields << field.Name
        end
        begin
            @data = recordset.GetRows.transpose
        rescue
            @data = []
        end
        recordset.Close
    end

    def execute(sql)
        @connection.Execute(sql)
    end

    def close
        @connection.Close
    end
end



begin

	db = AccessDb.new(@config['access_path'])
	db.open
		
		#complete query
		db.query("SELECT * FROM #{@config['proddata']} WHERE Status='5'and CompleteSync is not null and a_updated_at BETWEEN ##{Date.yesterday.strftime("%m/%d/%y")}# AND ##{Date.today.strftime("%m/%d/%y")}# ORDER BY Id")
			field_complete = db.fields
			rows_complete = db.data

			
	rescue Exception => e
		Log.error "Could Not Connect to MS Access Database.#{e}"
		Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
		$msg = "Subject: ProdData SQL Syncer Error \n\n\n #{e}.\n Cannot connect to MS Access Database. \n\n Date & Time: #{Time.now}"
		smtp.send_message($msg, $sender, $recipient)
		exit
end


#Connect to SQL Server		
ActiveRecord::Base.default_timezone = :local
ActiveRecord::Base.establish_connection(
	
	:adapter=>"sqlserver",
	:mode=>'dblib',
	:host=>@config['host'],
	:port=>@config['port'],
	:database=>@config['database'],
	:username=>@config['username'],
	:password=>@config['password'],
	:timeout=>@config['timeout'],
	:azure=>'false'

)

# Define Active records for SQL	
class Boo < ActiveRecord::Base

	self.table_name=$table_n
	self.primary_key=$primary_k

	
end



begin
Boo.connection
rescue Exception => e
	Log.error "Could Not Connect to SQL Server.#{e}"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Could not connect to SQL Server \n\n Date & Time: #{Time.now}"
	smtp.send_message($msg, $sender, $recipient)
	exit
end




def SQLBatchUpdate(accessData,accessdb)

begin

		batchUpdate=Boo.find_by(SQL_ID:accessData[77])	
		batchUpdate.update(SQL_ID:accessData[77],Arr_Date:accessData[0],Arr_Time:accessData[1],Variety_Code:accessData[2],Variety:accessData[3],MTicket:accessData[4],CDT_no:accessData[5],Q_No:accessData[6],SGuideNo:accessData[7],Truck_No:accessData[8],Trucker:accessData[9],Carrier_Type:accessData[10],Truck_spec:accessData[11],Van_No:accessData[12],Planter_Code:accessData[13],Barrio_Code:accessData[14],Driver_Code:accessData[15],GW_Date:accessData[16],GW_Time:accessData[17],Gross_Weight:accessData[18],Gross_Scaler:accessData[19],Original_GrossW:accessData[20],TW_Date:accessData[21],TW_Time:accessData[22],Tare_Weight:accessData[23],Tare_Scaler:accessData[24],SSCR_Date:accessData[25],SSCR_Time:accessData[26],Trash_percent:accessData[27],Burnt:accessData[28],Ded_Leaves:accessData[29],Ded_CaneTops:accessData[30],Ded_DeadStalks:accessData[31],Ded_WaterShoots:accessData[32],Ded_Muddy:accessData[33],Mill_No:accessData[34],Sample_No:accessData[35],Cane_Quality:accessData[36],Darami:accessData[37],Fejas_Date:accessData[38],Fejas_Time:accessData[39],Cor_Brix:accessData[40],POL_Reading:accessData[41],POL_percent:accessData[42],APRTY:accessData[43],PSTC:accessData[44],KSTC:accessData[45],LKGTC:accessData[46],Planters_Sugar:accessData[47],Planters_Molasses:accessData[48],Status:accessData[49],Carrier:accessData[50],Pending:accessData[51],Changed:accessData[52],Spillage:accessData[53],Spill_Code:accessData[54],SFactor:accessData[55],MFactor:accessData[56],Mode:accessData[57],Abono:accessData[58],Remarks:accessData[59],Reanalyzed_Date:accessData[60],CA:accessData[61],Type:accessData[62],Release_Date:accessData[63],Field_No:accessData[64],Dump_Location:accessData[65],Fiber:accessData[66],Retension:accessData[67],Extraction:accessData[68],AbsJuice:accessData[69],PolPercentCane:accessData[70],Final:accessData[71],Tag:accessData[72],Mill_Date:accessData[73],Mill_Time:accessData[74],Mill:accessData[75],SeqNo:accessData[76])
		
		Log.info "Batch update SQL_ID #{accessData[77]}"
		
		updatedatcur=batchUpdate.updated_at
		strdateupdate=updatedatcur.strftime("%m,%d,%y,%H,%M,%S").split(",")
		newdateupdate="#{strdateupdate[0]}/#{strdateupdate[1]}/#{strdateupdate[2]} #{strdateupdate[3]}:#{strdateupdate[4]}:#{strdateupdate[5]}"
		accessdb.execute("UPDATE #{@config['proddata']} SET a_updated_at='#{newdateupdate}' WHERE id=#{batchUpdate.SQL_ID};")
				
	rescue Exception => e
					Log.error "Error updating SQL/Access record.#{e}"
					Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
					#$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Failure  to update SQL/Access record with SQL_ID #{accessid}. \n\n Date & Time: #{Time.now}"
					#smtp.send_message($msg, $sender, $recipient)
					exit
end

end
 
 
for i in 0..(rows_complete.length-1)

	accessRecord=rows_complete[i]
	SQLBatchUpdate(accessRecord,db)

end
  
db.close

end

Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"





