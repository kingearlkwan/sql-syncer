#This program serves as media for transferring data from ProdData table of MS Access database to SQL database table.
#It creates and updates all records from MS Access to SQL.
#All created and updated records can be seen on the logfile


require 'win32ole'
require 'rubygems'
require 'active_record'
require 'activerecord-sqlserver-adapter'
require 'tiny_tds'
require 'log4r'
require 'win32API'
require 'net/smtp'
require 'mail'


#Log info and error during the program run
Log = Log4r::Logger.new("LogProdData")
Log.outputters << Log4r::Outputter.stderr
Dir.mkdir('log') unless Dir.exist?('log')
file = Log4r::FileOutputter.new('LogProdData',:filename=>'log/LogProdData.log')
file.formatter = Log4r::PatternFormatter.new(:pattern=>"[%l] %d :: %m")
Log.outputters << file



SW_HIDE = 0
WS_OVERLAPPED = 0
WS_DISABLED = 0x08000000
WS_MINIMIZED = 0x20000000

def already_running
	
	findwindow = Win32API.new('user32','FindWindow', ['p','p'], 'L')
	return true if findwindow.call("STATIC", "Rhi4FileProcessorHiddenWindowProd") != 0
	createwindow = Win32API.new('user32', 'CreateWindowEx', ['l', 'p','p', 'l', 'i', 'i', 'i', 'i', 
		'l', 'l', 'l', 'P'], 'l')
	dwStyle = (WS_DISABLED | WS_MINIMIZED)
	cw = createwindow.call( WS_OVERLAPPED, 'STATIC','Rhi4FileProcessorHiddenWindowProd', dwStyle, 0, 0, 0, 0, 0, 0, 0, 0 )
	show = Win32API.new('user32',  'ShowWindow', ['L', 'L'], 'L')
	show.call(cw, SW_HIDE)
	return false
end



if already_running
	Log.info "Sorry Application is already running!"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	exit
end



#YAML Configuration
begin

	@config=YAML.load_file Dir.pwd+'/config.yml'	
	
	
rescue Exception => e
	Log.error "Unable to find 'config.yml'"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	exit
end



# Email Notification set up for error
$recipient = @config['to']
$sender = @config['from']
$postemail = @config['postmaster']
$postpassword = @config['postpassword']


smtp = Net::SMTP.new 'smtp.gmail.com', 587
smtp.enable_starttls
smtp.start($postemail,$postemail,$postpassword,:login) do



#Set up MSaccess connection
$table_n=@config['tablename']
$primary_k=@config['primarykey']

class AccessDb
    attr_accessor :mdb, :connection, :data, :fields

    def initialize(mdb=nil)
        @mdb = mdb
        @connection = nil
        @data = nil
        @fields = nil
    end

    def open 
        connection_string =  'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='
        connection_string << @mdb
        @connection = WIN32OLE.new('ADODB.Connection')
        @connection.Open(connection_string)
    end

    def query(sql)
        recordset = WIN32OLE.new('ADODB.Recordset')
        recordset.Open(sql, @connection)
        @fields = []
        recordset.Fields.each do |field|
            @fields << field.Name
        end
        begin
            @data = recordset.GetRows.transpose
        rescue
            @data = []
        end
        recordset.Close
    end

    def execute(sql)
        @connection.Execute(sql)
    end

    def close
        @connection.Close
    end
end



begin

	db = AccessDb.new(@config['access_path'])
	db.open
		
		#Check number of records in MS ACCESS,Reference for record creation in sql
		db.query("SELECT * FROM #{@config['proddata']} WHERE a_created_at is null ORDER BY Id")
			field_names_all = db.fields
			rows_all = db.data
			
		#update query
		db.query("SELECT * FROM #{@config['proddata']} WHERE Status<>'5' or Status is null ORDER BY Id")
			field_update = db.fields
			rows_update = db.data
			
		#complete query
		db.query("SELECT * FROM #{@config['proddata']} WHERE Status='5' and CompleteSync is null ORDER BY Id")
			field_complete = db.fields
			rows_complete = db.data

			
	rescue Exception => e
		Log.error "Could Not Connect to MS Access Database.#{e}"
		Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
		$msg = "Subject: ProdData SQL Syncer Error \n\n\n #{e}.\n Cannot connect to MS Access Database. \n\n Date & Time: #{Time.now}"
		smtp.send_message($msg, $sender, $recipient)
		exit
end


#Connect to SQL Server		
ActiveRecord::Base.default_timezone = :local
ActiveRecord::Base.establish_connection(
	
	:adapter=>"sqlserver",
	:mode=>'dblib',
	:host=>@config['host'],
	:port=>@config['port'],
	:database=>@config['database'],
	:username=>@config['username'],
	:password=>@config['password'],
	:timeout=>@config['timeout'],
	:azure=>'false'

)

# Define Active records for SQL	
class Boo < ActiveRecord::Base

	self.table_name=$table_n
	self.primary_key=$primary_k

	
end



begin
Boo.connection
rescue Exception => e
	Log.error "Could Not Connect to SQL Server.#{e}"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Could not connect to SQL Server \n\n Date & Time: #{Time.now}"
	smtp.send_message($msg, $sender, $recipient)
	exit
end




def SQLupdate(accessdate,accesstime,updatedbaccess,accessdb)

begin
	if !accessdate.nil? and !accesstime.nil? 
		gendatearr=accessdate.strftime("%Y/%m/%d").split("/")#datetime month day yr as array
		gentime=accesstime.split(":")
		gendate = Date.new(gendatearr[0].to_i, gendatearr[1].to_i, gendatearr[2].to_i)
	    dtgen = DateTime.new(gendate.year, gendate.month, gendate.day,gentime[0].to_i,gentime[1].to_i,0).change(:offset => "+0800")
		genupdate = Boo.find_by(SQL_ID:updatedbaccess[77])
	
		updatedatprev=genupdate.updated_at
		
		if dtgen>updatedatprev
				
				genupdate.update(SQL_ID:updatedbaccess[77],Arr_Date:updatedbaccess[0],Arr_Time:updatedbaccess[1],Variety_Code:updatedbaccess[2],Variety:updatedbaccess[3],MTicket:updatedbaccess[4],CDT_no:updatedbaccess[5],Q_No:updatedbaccess[6],SGuideNo:updatedbaccess[7],Truck_No:updatedbaccess[8],Trucker:updatedbaccess[9],Carrier_Type:updatedbaccess[10],Truck_spec:updatedbaccess[11],Van_No:updatedbaccess[12],Planter_Code:updatedbaccess[13],Barrio_Code:updatedbaccess[14],Driver_Code:updatedbaccess[15],GW_Date:updatedbaccess[16],GW_Time:updatedbaccess[17],Gross_Weight:updatedbaccess[18],Gross_Scaler:updatedbaccess[19],Original_GrossW:updatedbaccess[20],TW_Date:updatedbaccess[21],TW_Time:updatedbaccess[22],Tare_Weight:updatedbaccess[23],Tare_Scaler:updatedbaccess[24],SSCR_Date:updatedbaccess[25],SSCR_Time:updatedbaccess[26],Trash_percent:updatedbaccess[27],Burnt:updatedbaccess[28],Ded_Leaves:updatedbaccess[29],Ded_CaneTops:updatedbaccess[30],Ded_DeadStalks:updatedbaccess[31],Ded_WaterShoots:updatedbaccess[32],Ded_Muddy:updatedbaccess[33],Mill_No:updatedbaccess[34],Sample_No:updatedbaccess[35],Cane_Quality:updatedbaccess[36],Darami:updatedbaccess[37],Fejas_Date:updatedbaccess[38],Fejas_Time:updatedbaccess[39],Cor_Brix:updatedbaccess[40],POL_Reading:updatedbaccess[41],POL_percent:updatedbaccess[42],APRTY:updatedbaccess[43],PSTC:updatedbaccess[44],KSTC:updatedbaccess[45],LKGTC:updatedbaccess[46],Planters_Sugar:updatedbaccess[47],Planters_Molasses:updatedbaccess[48],Status:updatedbaccess[49],Carrier:updatedbaccess[50],Pending:updatedbaccess[51],Changed:updatedbaccess[52],Spillage:updatedbaccess[53],Spill_Code:updatedbaccess[54],SFactor:updatedbaccess[55],MFactor:updatedbaccess[56],Mode:updatedbaccess[57],Abono:updatedbaccess[58],Remarks:updatedbaccess[59],Reanalyzed_Date:updatedbaccess[60],CA:updatedbaccess[61],Type:updatedbaccess[62],Release_Date:updatedbaccess[63],Field_No:updatedbaccess[64],Dump_Location:updatedbaccess[65],Fiber:updatedbaccess[66],Retension:updatedbaccess[67],Extraction:updatedbaccess[68],AbsJuice:updatedbaccess[69],PolPercentCane:updatedbaccess[70],Final:updatedbaccess[71],Tag:updatedbaccess[72],Mill_Date:updatedbaccess[73],Mill_Time:updatedbaccess[74],Mill:updatedbaccess[75],SeqNo:updatedbaccess[76])
		
				accessid=genupdate.SQL_ID
				updatedatcur=genupdate.updated_at
				strdateupdate=updatedatcur.strftime("%m,%d,%y,%H,%M,%S").split(",")
				newdateupdate="#{strdateupdate[0]}/#{strdateupdate[1]}/#{strdateupdate[2]} #{strdateupdate[3]}:#{strdateupdate[4]}:#{strdateupdate[5]}"

				accessdb.execute("UPDATE #{@config['proddata']} SET a_updated_at='#{newdateupdate}' WHERE id=#{accessid};")
				Log.info "updating SQL_ID #{accessid}"
			 
		end
	end
	
	rescue Exception => e
					Log.error "Error updating SQL record.#{e}"
					Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
					#$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Failure  to update SQL/Access record with SQL_ID #{accessid}. \n\n Date & Time: #{Time.now}"
					#smtp.send_message($msg, $sender, $recipient)
					exit
end

end
 

 begin
for i in (0)..(rows_all.length-1)
x=rows_all[i]
sqlCheck=Boo.find_by(SQL_ID:x[77])


if sqlCheck.eql? nil
	
	Boo.create(SQL_ID:x[77],Arr_Date:x[0],Arr_Time:x[1],Variety_Code:x[2],Variety:x[3],MTicket:x[4],CDT_no:x[5],Q_No:x[6],SGuideNo:x[7],Truck_No:x[8],Trucker:x[9],Carrier_Type:x[10],Truck_spec:x[11],Van_No:x[12],Planter_Code:x[13],Barrio_Code:x[14],Driver_Code:x[15],GW_Date:x[16],GW_Time:x[17],Gross_Weight:x[18],Gross_Scaler:x[19],Original_GrossW:x[20],TW_Date:x[21],TW_Time:x[22],Tare_Weight:x[23],Tare_Scaler:x[24],SSCR_Date:x[25],SSCR_Time:x[26],Trash_percent:x[27],Burnt:x[28],Ded_Leaves:x[29],Ded_CaneTops:x[30],Ded_DeadStalks:x[31],Ded_WaterShoots:x[32],Ded_Muddy:x[33],Mill_No:x[34],Sample_No:x[35],Cane_Quality:x[36],Darami:x[37],Fejas_Date:x[38],Fejas_Time:x[39],Cor_Brix:x[40],POL_Reading:x[41],POL_percent:x[42],APRTY:x[43],PSTC:x[44],KSTC:x[45],LKGTC:x[46],Planters_Sugar:x[47],Planters_Molasses:x[48],Status:x[49],Carrier:x[50],Pending:x[51],Changed:x[52],Spillage:x[53],Spill_Code:x[54],SFactor:x[55],MFactor:x[56],Mode:x[57],Abono:x[58],Remarks:x[59],Reanalyzed_Date:x[60],CA:x[61],Type:x[62],Release_Date:x[63],Field_No:x[64],Dump_Location:x[65],Fiber:x[66],Retension:x[67],Extraction:x[68],AbsJuice:x[69],PolPercentCane:x[70],Final:x[71],Tag:x[72],Mill_Date:x[73],Mill_Time:x[74],Mill:x[75],SeqNo:x[76],Subsidiary:@config['subsidiary'],Crop_Year:@config['cropyear'])
	sqldat=Boo.find_by(SQL_ID:x[77])
	Log.info "creating SQL_ID #{sqldat.SQL_ID}"
	
	recentCreate = Boo.find_by(SQL_ID:x[77])
	create=recentCreate.created_at
	accessid=recentCreate.SQL_ID
	strdate=create.strftime("%m,%d,%y,%H,%M,%S").split(",")
	newdate="#{strdate[0]}/#{strdate[1]}/#{strdate[2]} #{strdate[3]}:#{strdate[4]}:#{strdate[5]}"

	db.execute("UPDATE #{@config['proddata']} SET a_created_at='#{newdate}' WHERE id=#{accessid};")
	
else

	recentCreate = Boo.find_by(SQL_ID:x[77])
	create=recentCreate.created_at
	accessid=recentCreate.SQL_ID
	strdate=create.strftime("%m,%d,%y,%H,%M,%S").split(",")
	newdate="#{strdate[0]}/#{strdate[1]}/#{strdate[2]} #{strdate[3]}:#{strdate[4]}:#{strdate[5]}"

	db.execute("UPDATE #{@config['proddata']} SET a_created_at='#{newdate}' WHERE id=#{accessid};")
end
end

rescue Exception => e
					Log.error "Error on creation of SQL record.#{e}"
					Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
					#$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Failure  to update SQL/Access record with SQL_ID #{accessid}. \n\n Date & Time: #{Time.now}"
					#smtp.send_message($msg, $sender, $recipient)
					exit
end



 for i in (0)..(rows_update.length-1)
 
  accessupdate=rows_update[i]
  grossDate=accessupdate[16]
  grosstime=accessupdate[17]
  fejasDate=accessupdate[38]
  fejasTime=accessupdate[39]
  millDate=accessupdate[73]
  millTime=accessupdate[74]
  tareDate=accessupdate[21]
  tareTime=accessupdate[22]
  
  SQLupdate(grossDate,grosstime,accessupdate,db)
  SQLupdate(fejasDate,fejasTime,accessupdate,db)
  SQLupdate(millDate,millTime,accessupdate,db)
  SQLupdate(tareDate,tareTime,accessupdate,db)
  
end
   
   
begin
for i in 0..(rows_complete.length-1)

	accessupdate=rows_complete[i]
	completeupdate = Boo.find_by(SQL_ID:accessupdate[77])
	completeupdate.update(SQL_ID:accessupdate[77],Arr_Date:accessupdate[0],Arr_Time:accessupdate[1],Variety_Code:accessupdate[2],Variety:accessupdate[3],MTicket:accessupdate[4],CDT_no:accessupdate[5],Q_No:accessupdate[6],SGuideNo:accessupdate[7],Truck_No:accessupdate[8],Trucker:accessupdate[9],Carrier_Type:accessupdate[10],Truck_spec:accessupdate[11],Van_No:accessupdate[12],Planter_Code:accessupdate[13],Barrio_Code:accessupdate[14],Driver_Code:accessupdate[15],GW_Date:accessupdate[16],GW_Time:accessupdate[17],Gross_Weight:accessupdate[18],Gross_Scaler:accessupdate[19],Original_GrossW:accessupdate[20],TW_Date:accessupdate[21],TW_Time:accessupdate[22],Tare_Weight:accessupdate[23],Tare_Scaler:accessupdate[24],SSCR_Date:accessupdate[25],SSCR_Time:accessupdate[26],Trash_percent:accessupdate[27],Burnt:accessupdate[28],Ded_Leaves:accessupdate[29],Ded_CaneTops:accessupdate[30],Ded_DeadStalks:accessupdate[31],Ded_WaterShoots:accessupdate[32],Ded_Muddy:accessupdate[33],Mill_No:accessupdate[34],Sample_No:accessupdate[35],Cane_Quality:accessupdate[36],Darami:accessupdate[37],Fejas_Date:accessupdate[38],Fejas_Time:accessupdate[39],Cor_Brix:accessupdate[40],POL_Reading:accessupdate[41],POL_percent:accessupdate[42],APRTY:accessupdate[43],PSTC:accessupdate[44],KSTC:accessupdate[45],LKGTC:accessupdate[46],Planters_Sugar:accessupdate[47],Planters_Molasses:accessupdate[48],Status:accessupdate[49],Carrier:accessupdate[50],Pending:accessupdate[51],Changed:accessupdate[52],Spillage:accessupdate[53],Spill_Code:accessupdate[54],SFactor:accessupdate[55],MFactor:accessupdate[56],Mode:accessupdate[57],Abono:accessupdate[58],Remarks:accessupdate[59],Reanalyzed_Date:accessupdate[60],CA:accessupdate[61],Type:accessupdate[62],Release_Date:accessupdate[63],Field_No:accessupdate[64],Dump_Location:accessupdate[65],Fiber:accessupdate[66],Retension:accessupdate[67],Extraction:accessupdate[68],AbsJuice:accessupdate[69],PolPercentCane:accessupdate[70],Final:accessupdate[71],Tag:accessupdate[72],Mill_Date:accessupdate[73],Mill_Time:accessupdate[74],Mill:accessupdate[75],SeqNo:accessupdate[76])
	
	accessid=completeupdate.SQL_ID
	updatedatcur=completeupdate.updated_at
	strdateupdate=updatedatcur.strftime("%m,%d,%y,%H,%M,%S").split(",")
	newdateupdate="#{strdateupdate[0]}/#{strdateupdate[1]}/#{strdateupdate[2]} #{strdateupdate[3]}:#{strdateupdate[4]}:#{strdateupdate[5]}"

	db.execute("UPDATE #{@config['proddata']} SET a_updated_at='#{newdateupdate}',CompleteSync='Completed' WHERE id=#{accessid};")
	Log.info "updating last phase of SQL_ID #{accessid}"
end

rescue Exception => e
					Log.error "Error updating last phase of SQL record.#{e}"
					Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
					#$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Failure  to update SQL/Access record with SQL_ID #{accessid}. \n\n Date & Time: #{Time.now}"
					#smtp.send_message($msg, $sender, $recipient)
					exit
end
  
db.close

end

Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"





